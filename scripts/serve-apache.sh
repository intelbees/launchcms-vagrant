#!/usr/bin/env bash
#!/usr/bin/env bash

block="<VirtualHost *:80>
    ServerName $1
    DocumentRoot \"$2\"
    <Directory \"$2\">
    Require all granted
    AllowOverride All
   </Directory>

    ErrorLog /var/log/apache2/$1.log
    CustomLog /var/log/apache2/$1.access.log combined
</VirtualHost>"



echo "$block" > "/etc/apache2/sites-available/$1.conf"
ln -fs "/etc/apache2/sites-available/$1.conf" "/etc/apache2/sites-enabled/$1.conf"
service apache2 restart
