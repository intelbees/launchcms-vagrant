LaunchCMS Vagrant Box
==========
Launch  Vagrant Box is a preconfigured Vagrant Box with a full array of LAMP Stack features to get you up and running with Vagrant in minimum time.


## Features
### System Stuff

- Ubuntu 14.04 LTS (Trusty)
- PHP 5.6
- Vim
- Git
- cURL
- GD and Imagick
- Composer
- Node
- NPM
- Mcrypt
- ElasticSearch 
- Nginx

### Database Stuff
- MySQL
- MongoDB enterprise version

### Caching Stuff
- Redis
- Memcached

### Node Stuff
- Grunt
- Bower
- Yeoman
- Gulp
- Browsersync
- PM2
- Meteor


## Get Started

* Download and Install [Vagrant][1]
* Download and Install [VirtualBox][2]
* Clone the LaunchCMS Developer Box [GitLab Repository](https://gitlab.com/intelbees/launchcms-vagrant)
* Run ``` vagrant up ```
* Access Your Server  at  [http://192.168.20.10/][7]

## Basic Vagrant Commands
### Start or resume your server
```bash
vagrant up
```
### Pause your server
```bash
vagrant suspend
```
### Delete your server
```bash
vagrant destroy
```

### SSH into your server
```bash
vagrant ssh
```

## Database Access

### MySQL 

- Hostname: localhost or 192.168.20.10
- Username: homestead
- Password: secret

## How to use 
### Getting start
Clone the package
```sh
git clone git@gitlab.com:intelbees/launchcms-vagrant.git
```
Then, go the launchcms-vagrant folder.

The first step is running the init.sh (if you're on Linux/Mac computer) or init.bat file (if you're using Windows).

The final output of this action is: it will generate the hidden .homestead where you need to modify all necessary configuration.

The main configuration file is located at: .homestead/Homestead.yml. Please be noticed that you need to keep indentation exactly as the structure. 

### Configuring Shared Folders
The folders property of the Homestead.yaml file lists all of the folders you wish to share with your Homestead environment. As files within these folders are changed, they will be kept in sync between your local machine and the Homestead environment. You may configure as many shared folders as necessary:
```
folders:
    - map: ~/Code
      to: /home/vagrant/Code
```      
To enable NFS, just add a simple flag to your synced folder configuration:
```
folders:
    - map: ~/Code
      to: /home/vagrant/Code
      type: "nfs"
```

### Configuring Apache/Nginx Sites

Not familiar with Apache/Nginx? No problem. The sites property allows you to easily map a "domain" to a folder on your Homestead environment. A sample site configuration is included in the Homestead.yaml file. Again, you may add as many sites to your Homestead environment as necessary. Homestead can serve as a convenient, virtualized environment for every PHP project you are working on:
```
sites:
    - map: homestead.app
      to: /home/vagrant/Code/Laravel/public
      type: apache
```
Quick explanation:
* map: is the local domain name that you want to map to the website
* to: is the path to the source code inside the virtual machine. You should use the path (in map attribute of folders) that you made with Configuring Shared folder step.
* type: can be apache, nginx, hhvm. 

If you change the sites property after provisioning the Homestead box, you should re-run vagrant reload --provision to update the Apache/Nginx configuration on the virtual machine.

```
vagrant reload --provision
```

Currently, I removed Apache inside this box to reduce the number of package to be used and the size of the box. If you want to host your application on Apache, please install Apache after you up the box and related packages. To make life simpler, just need to run below commands:
```
# Install apache
sudo apt-get install apache2
sudo apt-get install libapache2-mod-php5 
# Enable apache2 run on boot
sudo update-rc.d apache2 enable
# Stop Nginx 
sudo service nginx stop
# Disable nginx on boot
sudo update-rc.d nginx disable
```

You should pick to use only Apache or Nginx to prevent conflicts. If you want to use both at the same time, you need to configure port for each to run. However, it will make your life complicated. It depends on you :). 

### The Hosts File
After doing above steps, you need to make your host computer to understand the domain you set. So, you need to modify your hosts file.
The host file in Windows computer is located at: Windows/System32/Drivers/etc/hosts

In Linux/Mac: /ect/hosts
Then you add the line: 

```
192.168.20.10  the_domain_you_put_in_homestead_yml
```

## Advanced things with LaunchCMS Vagrantbox

You can install other software or change completely the box for specific needs. Then, if you want to export this box to use, just run below command:

```
./create-box.sh
```
The output of this command is the file: launchcms-vagrant-box-new.boxtest. To use this box from local (without hosting anywhere), you just need to modify the file .homestead/Homestead.yml as below:

```
#box: "intelbees/launchcms-vagrant"
box: "file://launchcms-virtualbox-new.box"
#Comment the version to make it works. Vagrant does not support init localbox with version
#version: "1.0"
```




 [1]: https://www.vagrantup.com/downloads.html
 [2]: https://www.virtualbox.org/wiki/Downloads
 [3]: http://www.sequelpro.com/
 [4]: http://www.navicat.com/
 [5]: https://www.vagrantup.com/downloads.html
 [6]: https://www.virtualbox.org/wiki/Downloads
 [7]: http://192.168.20.10/
